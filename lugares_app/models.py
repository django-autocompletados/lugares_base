# -*- coding: utf8 -*-
from django.db import models

class Parroquias(models.Model):
    nombre                  = models.CharField(max_length=100,)
    municipio               = models.ForeignKey('Municipio')
    class Meta:
        db_table            = u'parroquia'
        verbose_name_plural = u'parroquias'
        verbose_name        = u'parroquia'
    def __unicode__(self):
        return self.nombre

class Municipio(models.Model):
    nombre                  = models.CharField(max_length=100)
    estado                  = models.ForeignKey('Estado')
    class Meta:
        db_table            = u'municipio'
        verbose_name_plural = u'municipios'
        verbose_name        = u'municipio'
    def __unicode__(self):
        return self.nombre

class Estado(models.Model):
    nombre                  = models.CharField(max_length=100)
    pais                    = models.ForeignKey('Pais',verbose_name=u'país')
    class Meta:
        db_table            = u'estado'
        verbose_name_plural = 'estados'
    def __unicode__(self):
        return u'%s'%(self.nombre)

class Pais(models.Model):
    nombre                  = models.CharField(max_length=50)
    class Meta:
    	db_table            = u'pais'
        #verbose_name_plural = 'paises'
        verbose_name_plural = u'países'
        verbose_name        = u'país'
    def __unicode__(self):
    	return "%s" % (self.nombre)


