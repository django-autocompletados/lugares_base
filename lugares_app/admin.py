# -*- coding: utf8 -*-
from django.contrib import admin
from lugares_app.models import *

admin.site.register(Parroquias)
admin.site.register(Municipio)
admin.site.register(Estado)
admin.site.register(Pais)
